FROM docker.io/library/golang:1.21 as build

WORKDIR /app

COPY ./go.mod .
COPY ./go.sum .

RUN go mod download

COPY ./main.go .

RUN CGO_ENABLED=0 go build -o example-go-http-app main.go

FROM docker.io/library/alpine:latest as server

WORKDIR /app

COPY --from=build /app/example-go-http-app ./

RUN chmod +x ./example-go-http-app

CMD [ "./example-go-http-app" ]
